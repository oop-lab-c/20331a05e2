//Program to demonstrate overriding in java

class parent{
    void age(int num){                  //Method of parent class
        System.out.println("Parent age "+ num);
    }
}

class child extends parent{
    void age(int num){                  //Method of child class
        System.out.println("Child age "+ num);
    }
}

public class MethodORJava {
   public static void main(String [] args){
        child obj = new child();
        obj.age(20);                //Overrides the function present in parent and gives output of child's function
   } 
}
