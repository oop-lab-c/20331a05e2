import java.util.Scanner;
// public inheritence
class Parent {
    private
     int z = 1;
     int getpvt()
       {
           return z;
       }
    protected 
     int y = 2;
     public
     int x = 3;

}
class Publicinherit extends Parent{
    int getpub(){
        return x;
        }
   
    int getprot(){
        return y;
        }
    
};
class Protectedinherit extends Parent{
    int getpub(){
        return x;
        }
        void print()
        {
            System.out.println("S V R");
        }
    
    int getprot(){
        return y;
        }
    
};
class Privateinherit extends Parent{
    //public
    int getpub(){
        return x;
        }
   
    int getprot(){
        return y;
        }
    
};
class Main{
    public static void main(String[] args){
    Publicinherit obj = new Publicinherit();
    System.out.println("PUBLIC INHERITENCE : \n");
    System.out.println("Public : "+obj.getpub());
    System.out.println("Private can't be accessed ");
    System.out.println("Protected : "+obj.getprot());
    Protectedinherit objprot= new Protectedinherit();
    System.out.println("\nPROTECTED INHERITENCE : \n");
    System.out.println("Public : "+objprot.getpub());
    System.out.println("Private can't be accessed ");
    System.out.println("Protected : "+objprot.getprot());
    objprot.print();
    Privateinherit objpvt = new Privateinherit();
    System.out.println("\nPRIVATE INHERITENCE : \n");
    System.out.println("Public : "+objpvt.getpub());
    System.out.println("Private can't be accessed ");
    System.out.println("Protected : "+objpvt.getprot());
    }
}
