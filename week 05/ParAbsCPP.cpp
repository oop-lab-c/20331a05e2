//Program to demonstrate partial abstraction
#include<iostream>
using namespace std;

class parent{
    public:
    void schoolchoice(){                         //Normal method of class
        cout<<"sri chaitanya school"<<endl;
    }
    virtual void collegechoice()=0;               //Virtual function
};
class son : public parent{
    public:
    void collegechoice(){
        cout<<"lendi Institute of technology"<<endl;
    }
};
class daughter : public parent{
    public:
    void collegechoice(){
        cout<<"MVGR College of Engineering"<<endl;
    } 
};
int main(){
    son s;
    daughter d;
    cout<<"Son's Name of School and College"<<endl;
    s.schoolchoice();               //defined in parent
    s.collegechoice();              //defined in class
    cout<<"Daughter's Name of School and College"<<endl;
    d.schoolchoice();               //defined in parent
    d.collegechoice();              //defined in class
}