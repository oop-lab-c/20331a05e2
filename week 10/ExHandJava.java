//Program to demonstrate Built-in Exception

import java.util.*;

 class ExHandJava
 {
   public static void main(String args[])
   { 
    System.out.println("Enter two integers");
    Scanner sc = new Scanner(System.in);
    try{
    int x = sc.nextInt();
    int y = sc.nextInt();
     try{
       int z = x/y;
       System.out.println("z = "+z);
      }catch(ArithmeticException e){
        System.out.println("Cant be divided with zero");
      }
    }catch(InputMismatchException e){
      System.out.println("Input must be of integer type");
    }
   }
 }


