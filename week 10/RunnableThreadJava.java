//Program to create a thread using Runnable interface

class MyThread implements Runnable{
    int num = 14;
    public void run(){
        for(int i=1;i<5;i++){
            System.out.println(num*i);
        }
    }
}
public class RunnableThreadJava {
    public static void main(String[] args){
        MyThread obj = new MyThread();
        Thread t = new Thread(obj);
        t.start();
    }
}

