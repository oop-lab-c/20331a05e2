//Program to create thread by extending Thread class

class MyThread extends Thread{                  //Class extending Thread class
    int variable=15;
    
    public void run(){                          // overriding  run method
         for(int i=0;i<variable;i++){
             System.out.println(i);
         }    
    }
}
class ThreadClassJava{
    public static void main(String[] args){
        MyThread obj = new MyThread();
        obj.start();                               //Calling  start method
    }
}