#include <iostream>
using namespace std;
class Animal {
   public:
    void eat() {
        cout << "It can eat!" << endl;
    }

    void sleep() {
        cout << "It can sleep!" << endl;
    }
};

// derived class
class Dog : public Animal {
 
   public:
    void bark() {
        cout << "It can bark!" << endl;
    }
};

int main() {
    // Create object of the Dog class
    Dog dog1;

    // Calling members of the base class
    dog1.eat();
    dog1.sleep();

    // Calling member of the derived class
    dog1.bark();
    return 0;
}