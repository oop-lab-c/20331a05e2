#include<iostream>
using namespace std;
class Vehicle {

public:
	Vehicle() 
    { 
        cout << "Constructor of Vehicle called" << endl; 
    }
};

class Car : public Vehicle {

public:
	Car() {
	cout<<"Constructor of Car called"<< endl;
	}
};

class Van : public Vehicle {

public:
	Van() {
		cout<<"Constructor of Van called"<< endl;
	}
};

class Fuel : public Car, public Van {
public:
	Fuel(){
		cout<<"Constructor of Fuel called"<< endl;
	}
};

int main() {
    Fuel obj;
}
