#include<iostream>
using namespace std;
class Vehicle {
  public:
    Vehicle()
    {
      cout << "This is a Vehicle\n";
    }
};
class FourWheeler{
    public:
    FourWheeler(){
        cout << "This is a 4 wheeler Vehicle\n";
    }

};
class simpleIn:public Vehicle{
    public:
    simpleIn(){
        cout<<"Base class:Vehicle"<<endl;
    }
    

};
class car:public Vehicle,public FourWheeler{
    public:
    car(){
        cout<<"Base class:Vehicle,FourWheeler"<<endl;
        cout<<"This is a car\n";
    }
   
};
class twoWheeler:public Vehicle{
    public:
    twoWheeler(){
        cout<<"Base Class:Vehicle\n";
        cout<<"this is a two wheeler Vehicle\n";
    }

};
class bike:public twoWheeler{
    public:
    bike(){
        cout<<"Base class:twoWheeler\n";
        cout<<"bike is a two wheeler and Vehicle\n";
    }

};
class bicycle:public twoWheeler{
    public:
    bicycle(){
        cout<<"Base class:twoWheeler\n";
        cout<<"bicycle is a two Wheeler and Vehicle\n";
    }
};
int main(){
    cout<<"Simple Inheritence"<<endl;
    simpleIn obj1;
    cout<<"multiple Inheritence\n";
    car obj2;
    cout<<"multilevel Inheritence\n";
    bike obj3;
    cout<<"heirarcial Inheritence\n";
    bike obj4;
    bicycle obj5;


}